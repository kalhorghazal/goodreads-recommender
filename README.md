# Goodreads-Recommender

In this project, I implemented three popular recommendation algorithms, including item‐based collaborative filtering, user‑based collaborative filtering, and probabilistic matrix factorization, for the recommender system of Goodreads.

[Here](https://github.com/zygmuntz/goodbooks-10k) is the dataset used in this project.
